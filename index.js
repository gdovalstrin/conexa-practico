const express = require('express');
var app = express();
const cors = require('cors');
const mongoose = require('mongoose');
const to = require('await-to-js').to;
const jwt = require('jsonwebtoken');
const axios = require('axios');
const bcrypt = require('bcrypt');

const User = require('./src/models/user.model');
var config = require('./src/configs/db.config');
const jwt_key = process.env.JWT_KEY;
const port = process.env.PORT || 3001;


app.use(cors());
app.use(express.json());

mongoose.connect(config.url);

app.get('/', (req, res) => {
    res.status(200).send('Practico conexa');
});

app.post('/api/register', async (req, res) => {
    console.log(req.body);

    let data = req.body;
    const newPassword = await bcrypt.hash(data.password, 10);
    let create_args = {
        name: data.name,
        email: data.email,
        password: newPassword
    };
    
    let [err, user] = await to(User.create(create_args));

    if (err) {
        res.send({ status: 'error', error: err });
        console.log(err);
    } else {
        res.json({ status: 'ok' });
    }
}); 

app.post('/api/login', async (req, res) => {
    console.log(req.body);

    let data = req.body;
    let findOne_args = {
        email: data.email
    };

    let [err, user] = await to(User.findOne(findOne_args));
    console.log(jwt_key);

    if (err) return res.send({ status: 'error', error: err });
    if (!user) {
        return res.send({ status: 'error', error: 'invalid email or password' });
    }

    const isPassword = await bcrypt.compare(data.password, user.password);
    
    if (isPassword) {
        const token = jwt.sign(
        {
            name: user.name,
            email: user.email
        }, jwt_key);
        return res.json({ status: 'ok', user: token });
    } else {
        return res.send({ status: 'error', user: false });
    }

}); 

app.get('/api/posts', async (req, res) => {
    console.log(req.body);
    const posts_url = 'https://jsonplaceholder.typicode.com/posts';

    const token = req.headers['x-access-token']

    try {
        let decoded = jwt.verify(token, jwt_key);
        let email = decoded.email;
        let user = await User.findOne({ email: email });

        axios.get(posts_url)
            .then(response => {
                console.log(response.data);
                console.log(typeof response.data);
                return res.json({ status: 'ok', posts: JSON.parse(JSON.stringify(response.data)) });
            })
            .catch(error => {
                console.log(error)
                return res.send({ status: 'error', error: error });
            })

    } catch(error) {
        console.log(error);
        res.send({ status: 'error', error: 'invalid token' });
    }
}); 

app.get('/api/photos', async (req, res) => {
    console.log(req.body);
    const photos_url = 'https://jsonplaceholder.typicode.com/photos';

    const token = req.headers['x-access-token']

    try {
        let decoded = jwt.verify(token, jwt_key);
        let email = decoded.email;
        let user = await User.findOne({ email: email });

        axios.get(photos_url)
            .then(response => {
                console.log(response.data);
                console.log(typeof response.data);
                return res.json({ status: 'ok', photos: JSON.parse(JSON.stringify(response.data)) });
            })
            .catch(error => {
                console.log(error)
                return res.send({ status: 'error', error: error });
            })

    } catch(error) {
        console.log(error);
        res.send({ status: 'error', error: 'invalid token' });
    }
}); 

app.listen(port, () => {
    console.log(`Server listening on port ̣̣${port}`);
});