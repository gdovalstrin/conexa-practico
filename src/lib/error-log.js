const httpError = require('http-errors');
const _ = require('lodash');

module.exports = function() {

    const CUSTOM_NAME = "Custom Error";
    this.CUSTOM_STATUS_CODE = 465;

    this.USER_CREATE = defaultErrorLog;
    this.USER_FIND = defaultErrorLog;
    this.USER_FIND_ONE = defaultErrorLog;
    this.USER_UPDATE = defaultErrorLog;


    this.USER_REGISTER_USER = function(data = {}) {
        let opts = {
            code: "user_0"
        };
      
        let error_message = "Cannot register user";
      
        let err = httpError(this.CUSTOM_STATUS_CODE, error_message, opts);
        err.name = CUSTOM_NAME;
      
        return err;
    }

    this.USER_LOGIN_USER = function(data = {}) {
        let opts = {
            code: "user_1"
        };
      
        let error_message = "Cannot login user";
      
        let err = httpError(this.CUSTOM_STATUS_CODE, error_message, opts);
        err.name = CUSTOM_NAME;
      
        return err;
    }







    function logErrorData(data){
        console.log(data);
        let custom_data = formatCustomData(data);
        return custom_data;
    }

    function formatCustomData(data){
        let custom_data;
        if (!_.isEmpty(data) || (data instanceof Error)){
            if (typeof(data) != "object" || Array.isArray(data)){
                custom_data = data;
            } else {
                if (data instanceof Error){
                    custom_data = JSON.stringify(data, Object.getOwnPropertyNames(data));
                } else {
                    Object.keys(data).forEach((prop) => {
                        if (data[prop] instanceof Error){
                            data[prop] = JSON.stringify(data[prop], Object.getOwnPropertyNames(data[prop]));
                        }
                    });
        
                    custom_data = data;
                }
            }
        }
    
        return custom_data;
    }

    function defaultErrorLog(err, data = {}){
        let error_data = logErrorData(data);
        // data formateada por si se manda a sentry
    
        err.statusCode = this.CUSTOM_STATUS_CODE;
        err.name = CUSTOM_NAME;
        return err;
    }
}