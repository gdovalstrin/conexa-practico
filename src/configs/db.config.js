const env = process.env;
const db = {
    host: env.DB_HOST,
    user: env.DB_USER,
    password: env.DB_PASSWORD,
    database: env.DB_NAME || 'user',
    port: env.DB_PORT || 27017,
    url: env.DB_URL || 'mongodb://localhost:27017/auth'
};

module.exports = db;